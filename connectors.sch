EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 4425 3550 2    50   Input ~ 0
AS_close_SDC
Text GLabel 4425 3350 2    50   Input ~ 0
AS_driving_mode
Text GLabel 4425 3450 2    50   Input ~ 0
TS_activation_cockpit
Text GLabel 4425 3250 2    50   Input ~ 0
TS_activation_external
Text GLabel 4425 3150 2    50   Input ~ 0
Watchdog
Text GLabel 4425 3050 2    50   Input ~ 0
Shutdown_circuit
Text GLabel 4425 2950 2    50   Input ~ 0
SDC_is_ready
Text GLabel 4425 2850 2    50   Input ~ 0
To_SDC_relays
$Comp
L power:+24V #PWR0118
U 1 1 605C6338
P 4425 2250
F 0 "#PWR0118" H 4425 2100 50  0001 C CNN
F 1 "+24V" V 4375 2350 50  0000 L CNN
F 2 "" H 4425 2250 50  0001 C CNN
F 3 "" H 4425 2250 50  0001 C CNN
	1    4425 2250
	0    1    1    0   
$EndComp
$Comp
L Regulator_Linear:L7805 U10
U 1 1 605C6F78
P 6650 2350
F 0 "U10" H 6650 2592 50  0000 C CNN
F 1 "L7805" H 6650 2501 50  0000 C CNN
F 2 "" H 6675 2200 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 6650 2300 50  0001 C CNN
	1    6650 2350
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0119
U 1 1 605CA0CC
P 6300 2350
F 0 "#PWR0119" H 6300 2200 50  0001 C CNN
F 1 "+24V" V 6315 2478 50  0000 L CNN
F 2 "" H 6300 2350 50  0001 C CNN
F 3 "" H 6300 2350 50  0001 C CNN
	1    6300 2350
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0120
U 1 1 605CA29A
P 7000 2300
F 0 "#PWR0120" H 7000 2150 50  0001 C CNN
F 1 "+5V" H 7015 2473 50  0000 C CNN
F 2 "" H 7000 2300 50  0001 C CNN
F 3 "" H 7000 2300 50  0001 C CNN
	1    7000 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0121
U 1 1 605CA932
P 6650 2775
F 0 "#PWR0121" H 6650 2525 50  0001 C CNN
F 1 "GND" H 6655 2602 50  0000 C CNN
F 2 "" H 6650 2775 50  0001 C CNN
F 3 "" H 6650 2775 50  0001 C CNN
	1    6650 2775
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 605CCD13
P 7000 2575
F 0 "C4" H 7115 2621 50  0000 L CNN
F 1 "10uF" H 7115 2530 50  0000 L CNN
F 2 "" H 7038 2425 50  0001 C CNN
F 3 "~" H 7000 2575 50  0001 C CNN
	1    7000 2575
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 605CDBE4
P 6300 2575
F 0 "C3" H 6050 2650 50  0000 L CNN
F 1 "100uF" H 5950 2575 50  0000 L CNN
F 2 "" H 6338 2425 50  0001 C CNN
F 3 "~" H 6300 2575 50  0001 C CNN
	1    6300 2575
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 2350 6300 2350
Wire Wire Line
	6300 2350 6300 2425
Connection ~ 6300 2350
Wire Wire Line
	7000 2300 7000 2350
Wire Wire Line
	7000 2350 6950 2350
Connection ~ 7000 2350
Wire Wire Line
	7000 2350 7000 2425
Wire Wire Line
	6300 2725 6650 2725
Wire Wire Line
	6650 2650 6650 2725
Connection ~ 6650 2725
Wire Wire Line
	6650 2725 7000 2725
Wire Wire Line
	6650 2725 6650 2775
$Comp
L power:GND #PWR0122
U 1 1 605DD4ED
P 4425 2350
F 0 "#PWR0122" H 4425 2100 50  0001 C CNN
F 1 "GND" V 4430 2222 50  0000 R CNN
F 2 "" H 4425 2350 50  0001 C CNN
F 3 "" H 4425 2350 50  0001 C CNN
	1    4425 2350
	0    -1   -1   0   
$EndComp
Text GLabel 4425 2750 2    50   Input ~ 0
Check_Actuator1
Text GLabel 4425 2650 2    50   Input ~ 0
Check_Actuator2
Text GLabel 4425 2550 2    50   Output ~ 0
EBS_Actuator1
Text GLabel 4425 2450 2    50   Output ~ 0
EBS_Actuator2
$Comp
L Regulator_Linear:L7808 U11
U 1 1 6077F415
P 8200 2375
F 0 "U11" H 8200 2617 50  0000 C CNN
F 1 "L7808" H 8200 2526 50  0000 C CNN
F 2 "" H 8225 2225 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 8200 2325 50  0001 C CNN
	1    8200 2375
	1    0    0    -1  
$EndComp
$Comp
L power:+24V #PWR0123
U 1 1 607831FE
P 7850 2375
F 0 "#PWR0123" H 7850 2225 50  0001 C CNN
F 1 "+24V" V 7865 2503 50  0000 L CNN
F 2 "" H 7850 2375 50  0001 C CNN
F 3 "" H 7850 2375 50  0001 C CNN
	1    7850 2375
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0124
U 1 1 6078320A
P 8200 2800
F 0 "#PWR0124" H 8200 2550 50  0001 C CNN
F 1 "GND" H 8205 2627 50  0000 C CNN
F 2 "" H 8200 2800 50  0001 C CNN
F 3 "" H 8200 2800 50  0001 C CNN
	1    8200 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 60783210
P 8550 2600
F 0 "C6" H 8665 2646 50  0000 L CNN
F 1 "10uF" H 8665 2555 50  0000 L CNN
F 2 "" H 8588 2450 50  0001 C CNN
F 3 "~" H 8550 2600 50  0001 C CNN
	1    8550 2600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 60783216
P 7850 2600
F 0 "C5" H 7600 2675 50  0000 L CNN
F 1 "100uF" H 7500 2600 50  0000 L CNN
F 2 "" H 7888 2450 50  0001 C CNN
F 3 "~" H 7850 2600 50  0001 C CNN
	1    7850 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 2375 7850 2375
Wire Wire Line
	7850 2375 7850 2450
Connection ~ 7850 2375
Wire Wire Line
	8550 2325 8550 2375
Wire Wire Line
	8550 2375 8500 2375
Connection ~ 8550 2375
Wire Wire Line
	8550 2375 8550 2450
Wire Wire Line
	7850 2750 8200 2750
Wire Wire Line
	8200 2675 8200 2750
Connection ~ 8200 2750
Wire Wire Line
	8200 2750 8550 2750
Wire Wire Line
	8200 2750 8200 2800
$Comp
L power:+8V #PWR0125
U 1 1 607867EC
P 8550 2325
F 0 "#PWR0125" H 8550 2175 50  0001 C CNN
F 1 "+8V" H 8565 2498 50  0000 C CNN
F 2 "" H 8550 2325 50  0001 C CNN
F 3 "" H 8550 2325 50  0001 C CNN
	1    8550 2325
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x23 J1
U 1 1 60787106
P 4225 2450
F 0 "J1" H 4143 1125 50  0000 C CNN
F 1 "Conn_01x23" H 4143 1216 50  0000 C CNN
F 2 "" H 4225 2450 50  0001 C CNN
F 3 "~" H 4225 2450 50  0001 C CNN
	1    4225 2450
	-1   0    0    1   
$EndComp
Text GLabel 4425 2050 2    50   Output ~ 0
Radio_1
Text GLabel 4425 1950 2    50   Output ~ 0
Radio_2
Text GLabel 4425 1850 2    50   Output ~ 0
Radio_3
Text GLabel 4425 1750 2    50   Output ~ 0
Radio_4
Text GLabel 4425 1650 2    50   BiDi ~ 0
EBS_RELAY_SIGNAL
Text GLabel 4425 1550 2    50   Input ~ 0
TSMS_AF_FAULTS
Text GLabel 4400 1450 2    50   Input ~ 0
EBS_"FAULT"
Wire Wire Line
	4425 1450 4400 1450
$EndSCHEMATC
