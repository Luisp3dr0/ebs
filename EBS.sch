EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 6225 1075 1900 1725
U 6052126A
F0 "Sheet60521269" 50
F1 "logic part.sch" 50
$EndSheet
$Sheet
S 1475 1075 1900 1725
U 6052118E
F0 "Sheet6052118D" 50
F1 "connectors.sch" 50
$EndSheet
$Sheet
S 3900 1075 1900 1725
U 605A424C
F0 "Sheet605A424B" 50
F1 "clock.sch" 50
$EndSheet
$Sheet
S 1450 3200 1800 1625
U 606B9898
F0 "Sheet606B9897" 50
F1 "mosfets.sch" 50
$EndSheet
$Sheet
S 4100 3425 1625 1325
U 6075169D
F0 "Sheet6075169C" 50
F1 "Receptor.sch" 50
$EndSheet
$Sheet
S 6100 3200 1900 1725
U 607E118E
F0 "Sheet607E118D" 50
F1 "Relays.sch" 50
$EndSheet
$EndSCHEMATC
