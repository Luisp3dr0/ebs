EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5450 1700 5525 1700
Wire Wire Line
	5000 1700 5150 1700
Wire Wire Line
	6275 1700 6275 2900
Wire Wire Line
	5825 1700 6275 1700
Connection ~ 6275 2900
Wire Wire Line
	5000 2900 5100 2900
Connection ~ 5000 2900
Wire Wire Line
	4875 2900 5000 2900
Wire Wire Line
	5000 1700 5000 2900
Wire Wire Line
	6025 2350 6025 2900
NoConn ~ 5825 3800
NoConn ~ 5825 3400
Connection ~ 5100 2900
Wire Wire Line
	5100 2900 5225 2900
Wire Wire Line
	6025 2900 6275 2900
Connection ~ 6025 2900
Connection ~ 6275 3200
Wire Wire Line
	6275 2900 6275 3200
Wire Wire Line
	5825 2900 6025 2900
Text GLabel 5825 3600 2    50   Input ~ 0
EBS_"FAULT"
Wire Wire Line
	5075 3700 5225 3700
$Comp
L power:+24V #PWR?
U 1 1 608A5A9A
P 5075 3700
AR Path="/6052118E/608A5A9A" Ref="#PWR?"  Part="1" 
AR Path="/607E118E/608A5A9A" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 5075 3550 50  0001 C CNN
F 1 "+24V" V 5090 3828 50  0000 L CNN
F 2 "" H 5075 3700 50  0001 C CNN
F 3 "" H 5075 3700 50  0001 C CNN
	1    5075 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5425 2350 5100 2350
Wire Wire Line
	5100 2900 5100 2350
$Comp
L Device:LED D?
U 1 1 608A2DA0
P 5675 1700
F 0 "D?" H 5668 1445 50  0000 C CNN
F 1 "LED" H 5668 1536 50  0000 C CNN
F 2 "" H 5675 1700 50  0001 C CNN
F 3 "~" H 5675 1700 50  0001 C CNN
	1    5675 1700
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 608A2D9A
P 5300 1700
F 0 "R?" V 5093 1700 50  0000 C CNN
F 1 "1K2" V 5184 1700 50  0000 C CNN
F 2 "" V 5230 1700 50  0001 C CNN
F 3 "~" H 5300 1700 50  0001 C CNN
	1    5300 1700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5725 2350 6025 2350
$Comp
L Diode:1N4148 D?
U 1 1 608A2D92
P 5575 2350
F 0 "D?" H 5575 2250 50  0000 C CNN
F 1 "1N4148" H 5575 2150 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5575 2175 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 5575 2350 50  0001 C CNN
	1    5575 2350
	-1   0    0    1   
$EndComp
$Comp
L Relay:FINDER-30.22 K?
U 1 1 6088C3FC
P 5525 3300
F 0 "K?" V 4775 3350 50  0000 C CNN
F 1 "FINDER-30.22" V 4849 3300 50  0000 C CNN
F 2 "Relay_THT:Relay_DPDT_Finder_30.22" H 6875 3270 50  0001 C CNN
F 3 "http://gfinder.findernet.com/assets/Series/354/S30EN.pdf" H 5525 3300 50  0001 C CNN
	1    5525 3300
	0    1    1    0   
$EndComp
Text GLabel 4875 2900 0    50   Input ~ 0
EBS_RELAY_SIGNAL
$Comp
L power:GND #PWR?
U 1 1 607E72D2
P 6275 3225
F 0 "#PWR?" H 6275 2975 50  0001 C CNN
F 1 "GND" H 6280 3052 50  0000 C CNN
F 2 "" H 6275 3225 50  0001 C CNN
F 3 "" H 6275 3225 50  0001 C CNN
	1    6275 3225
	1    0    0    -1  
$EndComp
Wire Wire Line
	5825 3200 6275 3200
Wire Wire Line
	6275 3200 6275 3225
Text GLabel 5225 3300 0    50   Input ~ 0
TSMS_AF_FAULTS
$EndSCHEMATC
